package sean.sleazy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/entity")
public class UserService {

    @GET
    @Path("/users/{id}")
    @Produces("application/json")
    public Response getUserById(@PathParam("id") Integer id) {
        User user = new User(1234L, "Sean", "Monaghan");
        return Response.status(200).entity(user).build();
    }

}