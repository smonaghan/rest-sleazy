package sean.sleazy;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "user")
public class User implements Serializable {

    @XmlAttribute(name = "id")
    private long id;

    @XmlElement(name = "firstName")
    private String fname;

    @XmlElement(name = "lastName")
    private String lname;

    public User(long id, String fname, String lname) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
    }

    public long getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (fname != null ? !fname.equals(user.fname) : user.fname != null) return false;
        if (lname != null ? !lname.equals(user.lname) : user.lname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fname != null ? fname.hashCode() : 0;
        result = 31 * result + (lname != null ? lname.hashCode() : 0);
        return result;
    }
}
